package com.phil.week9;
/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ){ 
        Rectangle rec = new Rectangle(5, 3);
        System.out.println(rec.toString());
        System.out.println(rec.calArea());
        System.out.println(rec.calPerimeter());

        Rectangle rec2 = new Rectangle(2, 2);
        System.out.println(rec2.toString());
        System.out.println(rec2.calArea());
        System.out.println(rec2.calPerimeter());

        Circle circle = new Circle(2);
        System.out.println(circle);
        System.out.printf("%s area: %.3f \n", circle.getName(), circle.calArea());
        System.out.printf("%s perimeter %.3f \n", circle.getName(), circle.calPerimeter());


        Circle circle2 = new Circle(3);
        System.out.println(circle2);
        System.out.printf("%s area: %.3f \n", circle2.getName(), circle.calArea());
        System.out.printf("%s perimeter %.3f \n", circle2.getName(), circle.calPerimeter());

        Triangle triangle1 = new Triangle(2, 3, 4);
        System.out.println(triangle1.toString());
        System.out.println(triangle1.calArea());
        System.out.println(triangle1.calPerimeter());

        Triangle triangle2 = new Triangle(4, 5, 6);
        System.out.println(triangle2.toString());
        System.out.println(triangle2.calArea());
        System.out.println(triangle2.calPerimeter());


    }
}
